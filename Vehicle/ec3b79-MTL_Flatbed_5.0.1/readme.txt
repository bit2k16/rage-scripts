Thanks for downloading MTL Flatbed Tow Truck Mod by I'm Not MentaL, Yoha, TheF3nt0n & SxY.
Please donate some bucks if you enjoy this mod.

<b>Features:</b>
- Working Bed
- Attach Vehicles to Flatbed (So it won't fall when driving)
- Dials
- Breakable Windows
- Hands on steering wheel
- Lightbar
- Liveries
- Tow Winch

<b>Requirements:</b>
- Latest ScriptHookV 
- Latest Community Script Hook V .NET 
- Visual C++ Redistributable for Visual Studio 2015 x64
- Minimum Microsoft .NET Framework 4.5.2 
- Flatbed Script
- OpenIV

<b>Install Add-On:</b>
1. If you already install previous version, please remove before proceed to next step (New installation skip this step).
2. Install Flatbed script.
3. Copy "update" folder to your GTAV directory (or mods folder).
4. Copy "scripts" folder to your GTAV directory.
5. Add this line <Item>dlcpacks:/flatbed3/</Item> into dlclist.xml
6. Spawn "flatbed3" with a trainer.

<b>Install Replace:</b>
1. Open up OpenIV.
2. Navigate to update > x64 > dlcpacks > patchday20ng > dlc.rpf > x64 > levels > gta5 > vehicles.rpf.
3. Drag and drop "flatbed.yft", "flatbed_hi.yft", "flatbed.ytd" and "flatbed+hi.ytd" into vehicles.rpf.
4. Copy "scripts" folder to your GTAV directory.
5. Spawn "flatbed" with a trainer.

<b>Changelog:</b>
v5.0.1
- Added Replace version.

v5.0
- New Flatbed model bugs fixed with LODs with working bed thanks to TheF3nt0n & SxY.
- Wipers feature removed due to R* fucked up the bombbay doors.
- Replaced roof Beacon with a lightbar attach on bed thanks to MrPrime.
- New liveries thanks to Yannerins and AlexanderLB.
- Added new winch system.
- Flatbed will not despawn whenever it is load/unloading vehicles.
- Removed Realistic Physics from v3.2 update due to people keep complaining it was a bug. *annoyed* 
- Fixed random Help notifications pops-up when other npc nearby.
- Removed ability to attach hook winch inside flatbed.

v4.0
- Re-wrote the script
- Fixed no respawn bug.
- Fixed Saving bug.
- Localized some language including: English, German, Spanish, Chinese Simplified, Chinese Traditional & Korean.

v3.3
- Major bugs fixes and improvements.

v3.2
- Script disables when on Mission, includes Director Mode
- Now your vehicle can be hook on any angles, any directions.
- Improved attach Physics, no more teleport, realism
- Can hook any vehicles when driving flatbed and with the bed dropped.
- Minor bugs fixes and improvements.

v3.1
- Fixed Wasted not being to respawn.

v3.0
- Roof Beacon added.
- Liveries added.
- Able to enter totaled vehicle when you stand close to it then hook to flatbed. (requested by Cyron43)
- Fixed back panel glitch when you spawn another flatbed.

v2.3
- Fixed Detach bugs from v2.2 update.
- Fixed despawn problem when entering flatbed.
- minor bugs fixed.

v2.2
- Fixed camera issue for most vehicles.
- Fixed lights issue on flatbed.

v2.1
- Fixed car bump/jump when detaching
- No more Blackout when attaching and detaching

v2.0
- Fixed Wipers
- Flatbed Slides Down Properly
- Ramp Removed
- Automatically Calculate the coordinates, no need to set manually on Flatbed.ini.
- All Vehicle Class added (Yes Police cars, Boats, Helicopters, Motorbikes, etc).

v1.1
- Script bugs fixed
- Attach/Detach Ramp can be do while in the flatbed.
- (Real) Add-On version

v1.0
- Initial Release

<b>Credits:</b>
Rockstar Games for the original model
Alexander Blade for ScriptHookV
Crosire for ScriptHookVDotNet
OpenIV Team for OpenIV
Yoha for the First model
TheF3nt0n for the Second model
SxY for the Third model
UnknownModder for Decor Unlocker
MrPrime for the Lightbars
Myself, Yannerins and AlexanderLB for Liveries
Myself for Script
glitchdetector for FiveM Script
Mell for Korean translation
Krazy! for Portuguese & Italian translation
Gixer for Spanish translation
pnda for German translation
��ѩ��0w0 for Japanese translation
Myself for Chinese translation