mp.events.add('Server:SetFunk', (player, id, channel) => {
	player.setVariable('FUNK_ID', id);
	player.setVariable('FUNK_CHANNEL', channel);
	player.notify(`Neue Frequenz: ~g~${id}~w~ Mhz~n~Neuer Channel: ~g~CH ${channel}~w~`);
});

mp.events.add('playerJoin', (player) => {
	player.call('Client:CreateFunk');
	player.setVariable('FUNK_STATE', false);
	player.setVariable('FUNK_ID', 00000);
	player.setVariable('FUNK_CHANNEL', 1);
});

mp.events.add('FUNK_STATE', (player, state) => {
	if(state == true){
		player.setVariable('FUNK_STATE', true);
		player.notify(`Dein Funk ist nun ~g~aktiv~w~.`);
		player.call('Client:PlayFunkAudio', [true]);
		if(player.vehicle) return;
	    player.playAnimation("random@arrests", "generic_radio_chatter", 3, 49);
	} else if(state == false){
		player.setVariable('FUNK_STATE', false);
		player.notify(`Dein Funk ist nun ~r~Standby~w~.`);
		player.call('Client:PlayFunkAudio', [false]);
		if(player.vehicle) return;
	    player.stopAnimation();
	}
});