﻿var FunkCef = null;
var FunkCefOpen = false;

mp.events.add('Client:CreateFunk', () => {
    if (FunkCef == null) {
        FunkCef = mp.browsers.new('package://cef/radio/index.html');
    }
});

mp.events.add("Client:SetFunk", (id, channel) => {
	mp.events.callRemote('Server:SetFunk', id, channel);
});

mp.events.add("Client:PlayFunkAudio", (state) => {
	FunkCef.execute("playRadio(" + state + ");");
});

mp.keys.bind(0x22, false, function () {
	if (FunkCefOpen == true) {
		FunkCef.execute("close();");
		mp.gui.cursor.visible = false;
		FunkCefOpen = false;
    } else if(FunkCefOpen == false){
		FunkCef.execute("open();");
		mp.gui.cursor.visible = true;
		FunkCefOpen = true;
	}
});

mp.events.add('Client:DeleteFunk', () => {
    if (FunkCef !== null) {
        FunkCef.destroy();
		FunkCef = null;
		FunkCefOpen = false;
    }
});

mp.keys.bind(0x28, true, function() {
    mp.events.callRemote('FUNK_STATE', true);
    player.playFacialAnim("mic_chatter", "mp_facial");
});

mp.keys.bind(0x28, false, function() {
    mp.events.callRemote('FUNK_STATE', false);
    player.playFacialAnim("mood_normal_1", "facials@gen_male@variations@normal");
});